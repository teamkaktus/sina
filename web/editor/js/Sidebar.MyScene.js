/**
 * @author mrdoob / http://mrdoob.com/
 */

Sidebar.MyScene = function (editor) {

    var signals = editor.signals;

    var container = new UI.Panel();
    //container.setBorderTop( '0' );
    //container.setPaddingTop( '20px' );

    // outliner

    function buildOption(object, draggable) {

        var option = document.createElement('div');
        option.draggable = draggable;
        option.innerHTML = buildHTML(object);
        option.value = object.id;

        return option;

    }

    function buildHTML(object) {

        var html = '<span class="type ' + object.type + '"></span> ' + object.name;

        if (object instanceof THREE.Mesh) {

            var geometry = object.geometry;
            var material = object.material;

            html += ' <span class="type ' + geometry.type + '"></span> ' + geometry.name;
            html += ' <span class="type ' + material.type + '"></span> ' + material.name;

        }

        html += getScript(object.uuid);

        return html;

    }

    function getScript(uuid) {

        if (editor.scripts[uuid] !== undefined) {

            return ' <span class="type Script"></span>';

        }

        return '';

    }

    var ignoreObjectSelectedSignal = false;

    var outliner = new UI.Outliner(editor);
    outliner.setId('outliner');
    outliner.onChange(function () {

        ignoreObjectSelectedSignal = true;

        editor.selectById(parseInt(outliner.getValue()));

        ignoreObjectSelectedSignal = false;

    });
    outliner.onDblClick(function () {

        editor.focusById(parseInt(outliner.getValue()));

    });
    //container.add( outliner );
    //container.add( new UI.Break() );

    /*
     // background

     var backgroundRow = new UI.Row();
     var background = new UI.Select().setOptions( {

     'None': 'None',
     'Color': 'Color',
     'Texture': 'Texture'

     } ).setWidth( '150px' );
     background.onChange( function () {} );

     backgroundRow.add( new UI.Text( 'Background' ).setWidth( '90px' ) );
     backgroundRow.add( background );

     container.add( backgroundRow );
     */

    // fog

    function updateFogParameters() {

        var near = fogNear.getValue();
        var far = fogFar.getValue();
        var density = fogDensity.getValue();

        signals.fogParametersChanged.dispatch(near, far, density);

    }

    var fogTypeRow = new UI.Row();
    var fogType = new UI.Select().setOptions({

        'None': 'None',
        'Fog': 'Linear',
        'FogExp2': 'Exponential'

    }).setWidth('150px');
    fogType.onChange(function () {

        var type = fogType.getValue();

        signals.fogTypeChanged.dispatch(type);

        refreshFogUI();

    });

    fogTypeRow.add(new UI.Text('Fog').setWidth('90px'));
    fogTypeRow.add(fogType);

    //container.add( fogTypeRow );

    // fog color

    var fogPropertiesRow = new UI.Row();
    fogPropertiesRow.setDisplay('none');
    fogPropertiesRow.setMarginLeft('90px');
    //container.add( fogPropertiesRow );

    var fogColor = new UI.Color().setValue('#aaaaaa');
    fogColor.onChange(function () {

        signals.fogColorChanged.dispatch(fogColor.getHexValue());

    });
    fogPropertiesRow.add(fogColor);

    // fog near

    var fogNear = new UI.Number(0.1).setWidth('40px').setRange(0, Infinity).onChange(updateFogParameters);
    fogPropertiesRow.add(fogNear);

    // fog far

    var fogFar = new UI.Number(100).setWidth('40px').setRange(0, Infinity).onChange(updateFogParameters);
    fogPropertiesRow.add(fogFar);

    // fog density

    var fogDensity = new UI.Number(0.00025).setWidth('40px').setRange(0, 0.1).setPrecision(5).onChange(updateFogParameters);
    fogPropertiesRow.add(fogDensity);

    //

    var refreshUI = function () {

        var camera = editor.camera;
        var scene = editor.scene;

        var options = [];

        //options.push( buildOption( camera, false ) );
        //options.push( buildOption( scene, false ) );

        (function addObjects(objects, pad) {

            for (var i = 0, l = objects.length; i < l; i++) {

                var object = objects[i];

                var option = buildOption(object, true);
                option.style.paddingLeft = ( pad * 10 ) + 'px';
                options.push(option);

                addObjects(object.children, pad + 1);

            }

        })(scene.children, 1);

        outliner.setOptions(options);

        if (editor.selected !== null) {

            outliner.setValue(editor.selected.id);

        }

        if (scene.fog) {

            fogColor.setHexValue(scene.fog.color.getHex());

            if (scene.fog instanceof THREE.Fog) {

                fogType.setValue("Fog");
                fogNear.setValue(scene.fog.near);
                fogFar.setValue(scene.fog.far);

            } else if (scene.fog instanceof THREE.FogExp2) {

                fogType.setValue("FogExp2");
                fogDensity.setValue(scene.fog.density);

            }

        } else {

            fogType.setValue("None");

        }

        refreshFogUI();

    };

    function refreshFogUI() {

        var type = fogType.getValue();

        fogPropertiesRow.setDisplay(type === 'None' ? 'none' : '');
        fogNear.setDisplay(type === 'Fog' ? '' : 'none');
        fogFar.setDisplay(type === 'Fog' ? '' : 'none');
        fogDensity.setDisplay(type === 'FogExp2' ? '' : 'none');

    }

    refreshUI();

    // events

    signals.sceneGraphChanged.add(refreshUI);

    signals.objectChanged.add(function (object) {

        var options = outliner.options;

        for (var i = 0; i < options.length; i++) {

            var option = options[i];

            if (option.value === object.id) {

                option.innerHTML = buildHTML(object);
                return;

            }

        }

    });

    signals.objectSelected.add(function (object) {

        if (ignoreObjectSelectedSignal === true) return;

        outliner.setValue(object !== null ? object.id : null);

    });
// Workspace size

    var sizeRow = new UI.Row();
    var buttons = new UI.Panel();
    sizeRow.add(buttons);
    container.add(sizeRow);

    var sizeLabel = new UI.Text('size').setWidth('50px');
    var unitLabel = new UI.Text('inch');

    var size = new UI.Input().setValue(editor.config.getKey('workspace_size')).setWidth('50px');
    size.dom.id = 'size';


    var setSize = new UI.Button('set size').onClick(function () {
        var val = document.getElementById('size').value * 10;
        if (val % 2 == 1) {
            val -= 1
        }
        editor.config.setKey('workspace_size', val / 10);
        editor.setSize(val / 10);
        console.log(val);

    });

    setSize.dom.style.margin = '0 0 0 10px';
    buttons.add(setSize);

    sizeRow.add(sizeLabel);
    sizeRow.add(size);
    sizeRow.add(unitLabel);
    sizeRow.add(setSize);

    container.add(sizeRow);

    // Actions

    var objectActions = new UI.Select().setPosition('absolute').setRight('8px').setFontSize('11px');
    objectActions.setOptions({

        'Actions': 'Actions',
        'Reset Position': 'Reset Position',
        'Reset Rotation': 'Reset Rotation',
        'Reset Scale': 'Reset Scale'

    });
    objectActions.onClick(function (event) {

        event.stopPropagation(); // Avoid panel collapsing

    });
    objectActions.onChange(function (event) {

        var object = editor.selected;

        switch (this.getValue()) {

            case 'Reset Position':
                editor.execute(new SetPositionCommand(object, new THREE.Vector3(0, 0, 0)));
                break;

            case 'Reset Rotation':
                editor.execute(new SetRotationCommand(object, new THREE.Euler(0, 0, 0)));
                break;

            case 'Reset Scale':
                editor.execute(new SetScaleCommand(object, new THREE.Vector3(1, 1, 1)));
                break;

        }

        this.setValue('Actions');

    });
    // container.addStatic( objectActions );

    // type

    var objectTypeRow = new UI.Row();
    var objectType = new UI.Text();

    objectTypeRow.add(new UI.Text('Type').setWidth('90px'));
    objectTypeRow.add(objectType);

    //container.add( objectTypeRow );

    // uuid

    var objectUUIDRow = new UI.Row();
    var objectUUID = new UI.Input().setWidth('115px').setFontSize('12px').setDisabled(true);
    var objectUUIDRenew = new UI.Button('⟳').setMarginLeft('7px').onClick(function () {

        objectUUID.setValue(THREE.Math.generateUUID());

        editor.execute(new SetUuidCommand(editor.selected, objectUUID.getValue()));

    });

    objectUUIDRow.add(new UI.Text('UUID').setWidth('90px'));
    objectUUIDRow.add(objectUUID);
    objectUUIDRow.add(objectUUIDRenew);

    //container.add( objectUUIDRow );

    // name

    var objectNameRow = new UI.Row();
    var objectName = new UI.Input().setWidth('150px').setFontSize('12px').onChange(function () {

        editor.execute(new SetValueCommand(editor.selected, 'name', objectName.getValue()));

    });

    objectNameRow.add(new UI.Text('Name').setWidth('90px'));
    objectNameRow.add(objectName);

    container.add(objectNameRow);

    // position

    var objectPositionRow = new UI.Row();
    var objectPositionX = new UI.Number().setWidth('50px').onChange(update);
    var objectPositionY = new UI.Number().setWidth('50px').onChange(update);
    var objectPositionZ = new UI.Number().setWidth('50px').onChange(update);

    objectPositionRow.add(new UI.Text('Position').setWidth('90px'));
    objectPositionRow.add(objectPositionX, objectPositionY, objectPositionZ);

    container.add(objectPositionRow);

    // rotation

    var objectRotationRow = new UI.Row();
    var objectRotationX = new UI.Number().setStep(10).setUnit('°').setWidth('50px').onChange(update);
    var objectRotationY = new UI.Number().setStep(10).setUnit('°').setWidth('50px').onChange(update);
    var objectRotationZ = new UI.Number().setStep(10).setUnit('°').setWidth('50px').onChange(update);

    objectRotationRow.add(new UI.Text('Rotation').setWidth('90px'));
    objectRotationRow.add(objectRotationX, objectRotationY, objectRotationZ);

    container.add(objectRotationRow);

    // scale

    var objectScaleRow = new UI.Row();
    var objectScaleLock = new UI.Checkbox(true).setPosition('absolute').setLeft('75px');
    var objectScaleX = new UI.Number(1).setRange(0.01, Infinity).setWidth('50px').onChange(updateScaleX);
    var objectScaleY = new UI.Number(1).setRange(0.01, Infinity).setWidth('50px').onChange(updateScaleY);
    var objectScaleZ = new UI.Number(1).setRange(0.01, Infinity).setWidth('50px').onChange(updateScaleZ);

    objectScaleRow.add(new UI.Text('Scale').setWidth('90px'));
    objectScaleRow.add(objectScaleLock);
    objectScaleRow.add(objectScaleX, objectScaleY, objectScaleZ);

    container.add(objectScaleRow);

    // fov

    var objectFovRow = new UI.Row();
    var objectFov = new UI.Number().onChange(update);

    objectFovRow.add(new UI.Text('Fov').setWidth('90px'));
    objectFovRow.add(objectFov);

    //container.add(objectFovRow);

    // near

    var objectNearRow = new UI.Row();
    var objectNear = new UI.Number().onChange(update);

    objectNearRow.add(new UI.Text('Near').setWidth('90px'));
    objectNearRow.add(objectNear);

    //container.add(objectNearRow);

    // far

    var objectFarRow = new UI.Row();
    var objectFar = new UI.Number().onChange(update);

    objectFarRow.add(new UI.Text('Far').setWidth('90px'));
    objectFarRow.add(objectFar);

    //container.add(objectFarRow);

    // intensity

    var objectIntensityRow = new UI.Row();
    var objectIntensity = new UI.Number().setRange(0, Infinity).onChange(update);

    objectIntensityRow.add(new UI.Text('Intensity').setWidth('90px'));
    objectIntensityRow.add(objectIntensity);

    //container.add(objectIntensityRow);

    // color

    var objectColorRow = new UI.Row();
    var objectColor = new UI.Color().onChange(update);

    objectColorRow.add(new UI.Text('Color').setWidth('90px'));
    objectColorRow.add(objectColor);

    //container.add(objectColorRow);

    // ground color

    var objectGroundColorRow = new UI.Row();
    var objectGroundColor = new UI.Color().onChange(update);

    objectGroundColorRow.add(new UI.Text('Ground color').setWidth('90px'));
    objectGroundColorRow.add(objectGroundColor);

    //container.add(objectGroundColorRow);

    // distance

    var objectDistanceRow = new UI.Row();
    var objectDistance = new UI.Number().setRange(0, Infinity).onChange(update);

    objectDistanceRow.add(new UI.Text('Distance').setWidth('90px'));
    objectDistanceRow.add(objectDistance);

    //container.add(objectDistanceRow);

    // angle

    var objectAngleRow = new UI.Row();
    var objectAngle = new UI.Number().setPrecision(3).setRange(0, Math.PI / 2).onChange(update);

    objectAngleRow.add(new UI.Text('Angle').setWidth('90px'));
    objectAngleRow.add(objectAngle);

    //container.add(objectAngleRow);

    // penumbra

    var objectPenumbraRow = new UI.Row();
    var objectPenumbra = new UI.Number().setRange(0, 1).onChange(update);

    objectPenumbraRow.add(new UI.Text('Penumbra').setWidth('90px'));
    objectPenumbraRow.add(objectPenumbra);

    //container.add(objectPenumbraRow);

    // decay

    var objectDecayRow = new UI.Row();
    var objectDecay = new UI.Number().setRange(0, Infinity).onChange(update);

    objectDecayRow.add(new UI.Text('Decay').setWidth('90px'));
    objectDecayRow.add(objectDecay);

    //container.add(objectDecayRow);

    // shadow

    var objectShadowRow = new UI.Row();

    objectShadowRow.add(new UI.Text('Shadow').setWidth('90px'));

    var objectCastShadow = new UI.THREE.Boolean(false, 'cast').onChange(update);
    objectShadowRow.add(objectCastShadow);

    var objectReceiveShadow = new UI.THREE.Boolean(false, 'receive').onChange(update);
    objectShadowRow.add(objectReceiveShadow);

    var objectShadowRadius = new UI.Number(1).onChange(update);
    objectShadowRow.add(objectShadowRadius);

    //container.add( objectShadowRow );

    // visible

    var objectVisibleRow = new UI.Row();
    var objectVisible = new UI.Checkbox().onChange(update);

    objectVisibleRow.add(new UI.Text('Visible').setWidth('90px'));
    objectVisibleRow.add(objectVisible);

    //container.add(objectVisibleRow);

    // user data

    var timeout;

    var objectUserDataRow = new UI.Row();
    var objectUserData = new UI.TextArea().setWidth('150px').setHeight('40px').setFontSize('12px').onChange(update);
    objectUserData.onKeyUp(function () {

        try {

            JSON.parse(objectUserData.getValue());

            objectUserData.dom.classList.add('success');
            objectUserData.dom.classList.remove('fail');

        } catch (error) {

            objectUserData.dom.classList.remove('success');
            objectUserData.dom.classList.add('fail');

        }

    });

    objectUserDataRow.add(new UI.Text('User data').setWidth('90px'));
    objectUserDataRow.add(objectUserData);

    //container.add( objectUserDataRow );

    //Toolbar moved here

    //var signals = editor.signals;


    var btnRow = new UI.Row();
    var buttons = new UI.Panel();
    btnRow.add(buttons);
    container.add(btnRow);

    // translate / rotate / scale

    var translate = new UI.Button('translate ( w )').onClick(function () {

        signals.transformModeChanged.dispatch('translate');

    });
    buttons.add(translate);

    var rotate = new UI.Button('rotate ( e )').onClick(function () {

        signals.transformModeChanged.dispatch('rotate');

    });
    buttons.add(rotate);

    var scale = new UI.Button('scale ( r )').onClick(function () {

        signals.transformModeChanged.dispatch('scale');

    });
    buttons.add(scale);

    var gridRow = new UI.Row();
    var gridButtons = new UI.Panel();
    gridRow.add(gridButtons);
    container.add(gridRow);

    // grid

    var grid = new UI.Number(1).setWidth('40px').onChange(update);
    gridButtons.add(new UI.Text('grid: '));
    gridButtons.add(grid);

    var snap = new UI.THREE.Boolean(false, 'snap').onChange(updateTool);
    gridButtons.add(snap);

    var local = new UI.THREE.Boolean(false, 'local').onChange(updateTool);
    //gridButtons.add( local );

    var showGrid = new UI.THREE.Boolean(true, 'show').onChange(function () {

        editor.sceneHelpers.visible = showGrid.getValue();

    });
    //gridButtons.add( showGrid );

    function updateTool() {

        signals.snapChanged.dispatch(snap.getValue() === true ? grid.getValue() : null);
        signals.spaceChanged.dispatch(local.getValue() === true ? "local" : "world");
        signals.showGridChanged.dispatch(showGrid.getValue());

    }

    //

    function updateScaleX() {

        var object = editor.selected;

        if (objectScaleLock.getValue() === true) {

            var scale = objectScaleX.getValue() / object.scale.x;

            objectScaleY.setValue(objectScaleY.getValue() * scale);
            objectScaleZ.setValue(objectScaleZ.getValue() * scale);

        }

        update();

    }

    function updateScaleY() {

        var object = editor.selected;

        if (objectScaleLock.getValue() === true) {

            var scale = objectScaleY.getValue() / object.scale.y;

            objectScaleX.setValue(objectScaleX.getValue() * scale);
            objectScaleZ.setValue(objectScaleZ.getValue() * scale);

        }

        update();

    }

    function updateScaleZ() {

        var object = editor.selected;

        if (objectScaleLock.getValue() === true) {

            var scale = objectScaleZ.getValue() / object.scale.z;

            objectScaleX.setValue(objectScaleX.getValue() * scale);
            objectScaleY.setValue(objectScaleY.getValue() * scale);

        }

        update();

    }

    function update() {

        var object = editor.selected;

        if (object !== null) {

            var newPosition = new THREE.Vector3(objectPositionX.getValue(), objectPositionY.getValue(), objectPositionZ.getValue());
            if (object.position.distanceTo(newPosition) >= 0.01) {

                editor.execute(new SetPositionCommand(object, newPosition));

            }

            var newRotation = new THREE.Euler(objectRotationX.getValue() * THREE.Math.DEG2RAD, objectRotationY.getValue() * THREE.Math.DEG2RAD, objectRotationZ.getValue() * THREE.Math.DEG2RAD);
            if (object.rotation.toVector3().distanceTo(newRotation.toVector3()) >= 0.01) {

                editor.execute(new SetRotationCommand(object, newRotation));

            }

            var newScale = new THREE.Vector3(objectScaleX.getValue(), objectScaleY.getValue(), objectScaleZ.getValue());
            if (object.scale.distanceTo(newScale) >= 0.01) {

                editor.execute(new SetScaleCommand(object, newScale));

            }

            if (object.fov !== undefined && Math.abs(object.fov - objectFov.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'fov', objectFov.getValue()));
                object.updateProjectionMatrix();

            }

            if (object.near !== undefined && Math.abs(object.near - objectNear.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'near', objectNear.getValue()));

            }

            if (object.far !== undefined && Math.abs(object.far - objectFar.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'far', objectFar.getValue()));

            }

            if (object.intensity !== undefined && Math.abs(object.intensity - objectIntensity.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'intensity', objectIntensity.getValue()));

            }

            if (object.color !== undefined && object.color.getHex() !== objectColor.getHexValue()) {

                editor.execute(new SetColorCommand(object, 'color', objectColor.getHexValue()));

            }

            if (object.groundColor !== undefined && object.groundColor.getHex() !== objectGroundColor.getHexValue()) {

                editor.execute(new SetColorCommand(object, 'groundColor', objectGroundColor.getHexValue()));

            }

            if (object.distance !== undefined && Math.abs(object.distance - objectDistance.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'distance', objectDistance.getValue()));

            }

            if (object.angle !== undefined && Math.abs(object.angle - objectAngle.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'angle', objectAngle.getValue()));

            }

            if (object.penumbra !== undefined && Math.abs(object.penumbra - objectPenumbra.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'penumbra', objectPenumbra.getValue()));

            }

            if (object.decay !== undefined && Math.abs(object.decay - objectDecay.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'decay', objectDecay.getValue()));

            }

            if (object.visible !== objectVisible.getValue()) {

                editor.execute(new SetValueCommand(object, 'visible', objectVisible.getValue()));

            }

            if (object.castShadow !== undefined && object.castShadow !== objectCastShadow.getValue()) {

                editor.execute(new SetValueCommand(object, 'castShadow', objectCastShadow.getValue()));

            }

            if (object.receiveShadow !== undefined && object.receiveShadow !== objectReceiveShadow.getValue()) {

                editor.execute(new SetValueCommand(object, 'receiveShadow', objectReceiveShadow.getValue()));
                object.material.needsUpdate = true;

            }

            if (object.shadow !== undefined) {

                if (object.shadow.radius !== objectShadowRadius.getValue()) {

                    editor.execute(new SetValueCommand(object.shadow, 'radius', objectShadowRadius.getValue()));

                }

            }

            try {

                var userData = JSON.parse(objectUserData.getValue());
                if (JSON.stringify(object.userData) != JSON.stringify(userData)) {

                    editor.execute(new SetValueCommand(object, 'userData', userData));

                }

            } catch (exception) {

                console.warn(exception);

            }

        }

    }

    function updateRows(object) {

        var properties = {
            'fov': objectFovRow,
            'near': objectNearRow,
            'far': objectFarRow,
            'intensity': objectIntensityRow,
            'color': objectColorRow,
            'groundColor': objectGroundColorRow,
            'distance': objectDistanceRow,
            'angle': objectAngleRow,
            'penumbra': objectPenumbraRow,
            'decay': objectDecayRow,
            'castShadow': objectShadowRow,
            'receiveShadow': objectReceiveShadow,
            'shadow': objectShadowRadius
        };

        for (var property in properties) {

            properties[property].setDisplay(object[property] !== undefined ? '' : 'none');

        }

    }

    function updateTransformRows(object) {

        if (object instanceof THREE.Light ||
            ( object instanceof THREE.Object3D && object.userData.targetInverse )) {

            objectRotationRow.setDisplay('none');
            objectScaleRow.setDisplay('none');

        } else {

            objectRotationRow.setDisplay('');
            objectScaleRow.setDisplay('');

        }

    }

    // events

    signals.objectSelected.add(function (object) {

        if (object !== null) {

            container.setDisplay('block');

            updateRows(object);
            updateUI(object);

        } else {

            container.setDisplay('none');

        }

    });

    signals.objectChanged.add(function (object) {

        if (object !== editor.selected) return;

        updateUI(object);

    });

    signals.refreshSidebarObject3D.add(function (object) {

        if (object !== editor.selected) return;

        updateUI(object);

    });

    function updateUI(object) {

        objectType.setValue(object.type);

        objectUUID.setValue(object.uuid);
        objectName.setValue(object.name);

        objectPositionX.setValue(object.position.x);
        objectPositionY.setValue(object.position.y);
        objectPositionZ.setValue(object.position.z);

        objectRotationX.setValue(object.rotation.x * THREE.Math.RAD2DEG);
        objectRotationY.setValue(object.rotation.y * THREE.Math.RAD2DEG);
        objectRotationZ.setValue(object.rotation.z * THREE.Math.RAD2DEG);

        objectScaleX.setValue(object.scale.x);
        objectScaleY.setValue(object.scale.y);
        objectScaleZ.setValue(object.scale.z);

        if (object.fov !== undefined) {

            objectFov.setValue(object.fov);

        }

        if (object.near !== undefined) {

            objectNear.setValue(object.near);

        }

        if (object.far !== undefined) {

            objectFar.setValue(object.far);

        }

        if (object.intensity !== undefined) {

            objectIntensity.setValue(object.intensity);

        }

        if (object.color !== undefined) {

            objectColor.setHexValue(object.color.getHexString());

        }

        if (object.groundColor !== undefined) {

            objectGroundColor.setHexValue(object.groundColor.getHexString());

        }

        if (object.distance !== undefined) {

            objectDistance.setValue(object.distance);

        }

        if (object.angle !== undefined) {

            objectAngle.setValue(object.angle);

        }

        if (object.penumbra !== undefined) {

            objectPenumbra.setValue(object.penumbra);

        }

        if (object.decay !== undefined) {

            objectDecay.setValue(object.decay);

        }

        if (object.castShadow !== undefined) {

            objectCastShadow.setValue(object.castShadow);

        }

        if (object.receiveShadow !== undefined) {

            objectReceiveShadow.setValue(object.receiveShadow);

        }

        if (object.shadow !== undefined) {

            objectShadowRadius.setValue(object.shadow.radius);

        }

        objectVisible.setValue(object.visible);

        try {

            objectUserData.setValue(JSON.stringify(object.userData, null, '  '));

        } catch (error) {

            console.log(error);

        }

        objectUserData.setBorderColor('transparent');
        objectUserData.setBackgroundColor('');

        updateTransformRows(object);

    }

    var alignRow = new UI.Row();
    var alignButtons = new UI.Panel();
    alignRow.add(alignButtons);
    container.add(alignRow);

    // alignXY
    var alignXY = new UI.Button('alignXY').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.z = ((box.size()).z / 2 - workspace_size / 2 + editor.selected.position.z - box.center().z);

        objectPositionZ.setValue(editor.selected.position.Z);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignButtons.add(alignXY);

    // alignXZ
    var alignXZ = new UI.Button('alignXZ').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.y = ((box.size().y / 2) + editor.selected.position.y - box.center().y);

        objectPositionY.setValue(editor.selected.position.x);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignButtons.add(alignXZ);

    // alignYZ
    var alignYZ = new UI.Button('alignYZ').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.x = ((box.size()).x / 2 - workspace_size / 2 + editor.selected.position.x - box.center().x);

        objectPositionX.setValue(editor.selected.position.x);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignButtons.add(alignYZ);

    // alignCenter
    var alignCenter = new UI.Button('alignCenter').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.x = (0 + editor.selected.position.x - box.center().x);
        editor.selected.position.y = (workspace_size / 2 + editor.selected.position.y - box.center().y);
        editor.selected.position.z = (0 + editor.selected.position.z - box.center().z);

        objectPositionX.setValue(editor.selected.position.x);
        objectPositionY.setValue(editor.selected.position.y);
        objectPositionZ.setValue(editor.selected.position.z);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignButtons.add(alignCenter);

    //scale panel
    var scaleTWRow = new UI.Row();
    var scaleTWButtons = new UI.Panel();
    scaleTWRow.add(scaleTWButtons);
    container.add(scaleTWRow);

    // scaleToWorkspace
    var scaleTW = new UI.Button('scaleTW').onClick(function () {
        var newScale = 1;
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);


        var box = new THREE.Box3().setFromObject(editor.selected);

        var dim = box.size();
        var dim_compare = {};
        dim_compare.key = 'x';
        dim_compare.value = dim.x;

        if (dim.x < dim.y) {
            dim_compare.key = 'y';
            dim_compare.value = dim.y;
        }

        if (dim.z > dim_compare.value) {
            dim_compare.key = 'z';
            dim_compare.value = dim.z;
        }

        newScale = workspace_size / dim_compare.value;

        editor.selected.scale.x *= newScale;
        editor.selected.scale.y *= newScale;
        editor.selected.scale.z *= newScale;

        var box = new THREE.Box3().setFromObject(editor.selected);

        editor.selected.position.x = (0 + editor.selected.position.x - box.center().x);
        editor.selected.position.y = ((box.size().y / 2) + editor.selected.position.y - box.center().y);
        editor.selected.position.z = (0 + editor.selected.position.z - box.center().z);

        objectScaleX.setValue(newScale);
        objectScaleY.setValue(newScale);
        objectScaleZ.setValue(newScale);

        editor.execute(new SetScaleCommand(editor.selected, new THREE.Vector3(editor.selected.scale.x, editor.selected.scale.y, editor.selected.scale.z)));

    });
    scaleTWButtons.add(scaleTW);

    // custom editor panel
    var editRow = new UI.Row();
    var editButtons = new UI.Panel();
    editRow.add(editButtons);
   // container.add(editRow);

    function sceneWalker(scene, MyFunc) {
        var i = 0;
        while (scene.children[i]) {
            if (scene.children[i].children[0] !== undefined) {
                sceneWalker(scene.children[i], MyFunc);
            } else {
                if (editor.selected.id != scene.children[i].id) {
                    MyFunc(editor.selected, scene.children[i]);
                }
            }
            i++;
        }
        return;

    }

    //substract - remove selected geometry from all intercepred geometries

    function substruct(source, target) {
        var target_bsp = new ThreeBSP(target);
        var source_bsp = new ThreeBSP(source);
        var result_bsp = target_bsp.subtract(source_bsp);
        var result = result_bsp.toMesh();
        result.geometry.computeVertexNormals();
        result.geometry.center();

        target.geometry = result.geometry;

    }

    var substractButton = new UI.Button('substract').onClick(function () {

        sceneWalker(editor.scene, substruct);

    });
    editButtons.add(substractButton);

    //union
    function union(scene) {

        var result;
        var i = 0;
        while (scene.children[i]) {
            if(scene.children[i] instanceof THREE.Group){
                result = union(scene.children[i]);
            }else{
                if(result === undefined){
                    var tmp_mesh = scene.children[i];
                    //tmp_mesh.geometry.applyMatrix(new THREE.Matrix4());

                    result = new ThreeBSP(tmp_mesh);
                }else{

                    var tmp_mesh = scene.children[i];
                   // tmp_mesh.geometry.applyMatrix(new THREE.Matrix4());

                    var target_bsp = new ThreeBSP(tmp_mesh);
                    result = result.union(target_bsp);
                }
            }
            i++;
        }
        return result;
    }

    var unionButton = new UI.Button('union').onClick(function () {

        if (editor.selected instanceof THREE.Group) {

            var result = union(editor.selected);
            result = result.toMesh();

            result.geometry.computeVertexNormals();
            result.geometry.center();

            var mesh = new THREE.Mesh( result.geometry, new THREE.MeshStandardMaterial() );
            mesh.name = 'Union';

            editor.execute(new SetScaleCommand(mesh, new THREE.Vector3(editor.selected.scale.x, editor.selected.scale.y, editor.selected.scale.z)));

            editor.execute( new AddObjectCommand( mesh ) );
        } else {
            alert("Select group for union");
        }

    });
    editButtons.add(unionButton);

    //intersect - save intersected parts om all models

    function intersect(source, target) {
        var target_bsp = new ThreeBSP(target);
        var source_bsp = new ThreeBSP(source);
        var result_bsp = target_bsp.intersect(source_bsp);
        var result = result_bsp.toMesh();
        result.geometry.computeVertexNormals();
        result.geometry.center();

        target.geometry = result.geometry;

    }

    var intersectButton = new UI.Button('intersect').onClick(function () {

        sceneWalker(editor.scene, intersect);

    });
    //editButtons.add(intersectButton);

    return container;

};
