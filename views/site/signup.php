<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="col-lg-6 col-lg-offset-3 form-box">
        <h1>Signup</h1>

        <?php $form = ActiveForm::begin(['id' => 'form-signup',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-md-10\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-md-2 control-label label-left'],
            ],

        ]); ?>
        <?= $form->field($newModel, 'username') ?>
        <?= $form->field($newModel, 'email') ?>
        <?= $form->field($newModel, 'password')->passwordInput() ?>

        <div class="col-md-offset-2 col-md-10">
            <?= Html::submitButton('Signup', ['class' => 'btn btn-primary']) ?>
        </div>
        <div style="color:#999;margin:1em 0">
            If you are already a member <?= Html::a('Login', ['site/login']) ?>.
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
