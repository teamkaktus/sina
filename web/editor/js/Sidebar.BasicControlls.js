/**
 * Created by lexlazzy on 10.08.2016.
 */
Sidebar.BasicControlls = function (editor) {

    var signals = editor.signals;

    var container = new UI.Panel();
    container.setBorderTop('0');
    container.setPaddingTop('20px');
    container.setDisplay('none');

    // Actions

    var objectActions = new UI.Select().setPosition('absolute').setRight('8px').setFontSize('11px');
    objectActions.setOptions({

        'Actions': 'Actions',
        'Reset Position': 'Reset Position',
        'Reset Rotation': 'Reset Rotation',
        'Reset Scale': 'Reset Scale'

    });
    objectActions.onClick(function (event) {

        event.stopPropagation(); // Avoid panel collapsing

    });
    objectActions.onChange(function (event) {

        var object = editor.selected;

        switch (this.getValue()) {

            case 'Reset Position':
                editor.execute(new SetPositionCommand(object, new THREE.Vector3(0, 0, 0)));
                break;

            case 'Reset Rotation':
                editor.execute(new SetRotationCommand(object, new THREE.Euler(0, 0, 0)));
                break;

            case 'Reset Scale':
                editor.execute(new SetScaleCommand(object, new THREE.Vector3(1, 1, 1)));
                break;

        }

        this.setValue('Actions');

    });
    // container.addStatic( objectActions );

    // type

    var objectTypeRow = new UI.Row();
    var objectType = new UI.Text();

    objectTypeRow.add(new UI.Text('Type').setWidth('90px'));
    objectTypeRow.add(objectType);

    //container.add( objectTypeRow );

    // uuid

    var objectUUIDRow = new UI.Row();
    var objectUUID = new UI.Input().setWidth('115px').setFontSize('12px').setDisabled(true);
    var objectUUIDRenew = new UI.Button('⟳').setMarginLeft('7px').onClick(function () {

        objectUUID.setValue(THREE.Math.generateUUID());

        editor.execute(new SetUuidCommand(editor.selected, objectUUID.getValue()));

    });

    objectUUIDRow.add(new UI.Text('UUID').setWidth('90px'));
    objectUUIDRow.add(objectUUID);
    objectUUIDRow.add(objectUUIDRenew);

    //container.add( objectUUIDRow );

    // name

    var objectNameRow = new UI.Row();
    var objectName = new UI.Input().setWidth('150px').setFontSize('12px').onChange(function () {

        editor.execute(new SetValueCommand(editor.selected, 'name', objectName.getValue()));

    });

    objectNameRow.add(new UI.Text('Name').setWidth('90px'));
    objectNameRow.add(objectName);

    //container.add(objectNameRow);

    // position

    var objectPositionRow = new UI.Row();
    var objectPositionX = new UI.Number().setWidth('50px').onChange(update);
    var objectPositionY = new UI.Number().setWidth('50px').onChange(update);
    var objectPositionZ = new UI.Number().setWidth('50px').onChange(update);

    objectPositionRow.add(new UI.Text('Position').setWidth('90px'));
    objectPositionRow.add(objectPositionX, objectPositionY, objectPositionZ);

    //container.add(objectPositionRow);

    // rotation

    var objectRotationRow = new UI.Row();
    var objectRotationX = new UI.Number().setStep(10).setUnit('°').setWidth('50px').onChange(update);
    var objectRotationY = new UI.Number().setStep(10).setUnit('°').setWidth('50px').onChange(update);
    var objectRotationZ = new UI.Number().setStep(10).setUnit('°').setWidth('50px').onChange(update);

    objectRotationRow.add(new UI.Text('Rotation').setWidth('90px'));
    objectRotationRow.add(objectRotationX, objectRotationY, objectRotationZ);

    //container.add(objectRotationRow);

    // scale

    var objectScaleRow = new UI.Row();
    var objectScaleLock = new UI.Checkbox(true).setPosition('absolute').setLeft('75px');
    var objectScaleX = new UI.Number(1).setRange(0.01, Infinity).setWidth('50px').onChange(updateScaleX);
    var objectScaleY = new UI.Number(1).setRange(0.01, Infinity).setWidth('50px').onChange(updateScaleY);
    var objectScaleZ = new UI.Number(1).setRange(0.01, Infinity).setWidth('50px').onChange(updateScaleZ);

    objectScaleRow.add(new UI.Text('Scale').setWidth('90px'));
    objectScaleRow.add(objectScaleLock);
    objectScaleRow.add(objectScaleX, objectScaleY, objectScaleZ);

    //container.add(objectScaleRow);

    // fov

    var objectFovRow = new UI.Row();
    var objectFov = new UI.Number().onChange(update);

    objectFovRow.add(new UI.Text('Fov').setWidth('90px'));
    objectFovRow.add(objectFov);

    //container.add(objectFovRow);

    // near

    var objectNearRow = new UI.Row();
    var objectNear = new UI.Number().onChange(update);

    objectNearRow.add(new UI.Text('Near').setWidth('90px'));
    objectNearRow.add(objectNear);

    //container.add(objectNearRow);

    // far

    var objectFarRow = new UI.Row();
    var objectFar = new UI.Number().onChange(update);

    objectFarRow.add(new UI.Text('Far').setWidth('90px'));
    objectFarRow.add(objectFar);

    //container.add(objectFarRow);

    // intensity

    var objectIntensityRow = new UI.Row();
    var objectIntensity = new UI.Number().setRange(0, Infinity).onChange(update);

    objectIntensityRow.add(new UI.Text('Intensity').setWidth('90px'));
    objectIntensityRow.add(objectIntensity);

    //container.add(objectIntensityRow);

    // color

    var objectColorRow = new UI.Row();
    var objectColor = new UI.Color().onChange(update);

    objectColorRow.add(new UI.Text('Color').setWidth('90px'));
    objectColorRow.add(objectColor);

    //container.add(objectColorRow);

    // ground color

    var objectGroundColorRow = new UI.Row();
    var objectGroundColor = new UI.Color().onChange(update);

    objectGroundColorRow.add(new UI.Text('Ground color').setWidth('90px'));
    objectGroundColorRow.add(objectGroundColor);

    //container.add(objectGroundColorRow);

    // distance

    var objectDistanceRow = new UI.Row();
    var objectDistance = new UI.Number().setRange(0, Infinity).onChange(update);

    objectDistanceRow.add(new UI.Text('Distance').setWidth('90px'));
    objectDistanceRow.add(objectDistance);

    //container.add(objectDistanceRow);

    // angle

    var objectAngleRow = new UI.Row();
    var objectAngle = new UI.Number().setPrecision(3).setRange(0, Math.PI / 2).onChange(update);

    objectAngleRow.add(new UI.Text('Angle').setWidth('90px'));
    objectAngleRow.add(objectAngle);

    //container.add(objectAngleRow);

    // penumbra

    var objectPenumbraRow = new UI.Row();
    var objectPenumbra = new UI.Number().setRange(0, 1).onChange(update);

    objectPenumbraRow.add(new UI.Text('Penumbra').setWidth('90px'));
    objectPenumbraRow.add(objectPenumbra);

    //container.add(objectPenumbraRow);

    // decay

    var objectDecayRow = new UI.Row();
    var objectDecay = new UI.Number().setRange(0, Infinity).onChange(update);

    objectDecayRow.add(new UI.Text('Decay').setWidth('90px'));
    objectDecayRow.add(objectDecay);

    //container.add(objectDecayRow);

    // shadow

    var objectShadowRow = new UI.Row();

    objectShadowRow.add(new UI.Text('Shadow').setWidth('90px'));

    var objectCastShadow = new UI.THREE.Boolean(false, 'cast').onChange(update);
    objectShadowRow.add(objectCastShadow);

    var objectReceiveShadow = new UI.THREE.Boolean(false, 'receive').onChange(update);
    objectShadowRow.add(objectReceiveShadow);

    var objectShadowRadius = new UI.Number(1).onChange(update);
    objectShadowRow.add(objectShadowRadius);

    //container.add( objectShadowRow );

    // visible

    var objectVisibleRow = new UI.Row();
    var objectVisible = new UI.Checkbox().onChange(update);

    objectVisibleRow.add(new UI.Text('Visible').setWidth('90px'));
    objectVisibleRow.add(objectVisible);

    //container.add(objectVisibleRow);

    // user data

    var timeout;

    var objectUserDataRow = new UI.Row();
    var objectUserData = new UI.TextArea().setWidth('150px').setHeight('40px').setFontSize('12px').onChange(update);
    objectUserData.onKeyUp(function () {

        try {

            JSON.parse(objectUserData.getValue());

            objectUserData.dom.classList.add('success');
            objectUserData.dom.classList.remove('fail');

        } catch (error) {

            objectUserData.dom.classList.remove('success');
            objectUserData.dom.classList.add('fail');

        }

    });

    objectUserDataRow.add(new UI.Text('User data').setWidth('90px'));
    objectUserDataRow.add(objectUserData);

    //container.add( objectUserDataRow );

    //Toolbar moved here

    //var signals = editor.signals;


    var btnRow = new UI.Row();
    var transformationText = new UI.Text("Transformation");
    var buttons = new UI.Panel();
    btnRow.add(transformationText);
    btnRow.add(buttons);
    container.add(btnRow);

    // translate / rotate / scale

    var translate = new UI.Button('').onClick(function () {

        signals.transformModeChanged.dispatch('translate');

    });
    translate.setId('translateBtn');
    buttons.add(translate);

    var rotate = new UI.Button('').onClick(function () {

        signals.transformModeChanged.dispatch('rotate');

    });
    rotate.setId('rotateBtn');
    buttons.add(rotate);

    var scale = new UI.Button('').onClick(function () {

        signals.transformModeChanged.dispatch('scale');

    });
    scale.setId('scaleBtn');
    buttons.add(scale);

    var gridRow = new UI.Row();
    var gridButtons = new UI.Panel();
    gridRow.add(gridButtons);
    container.add(gridRow);

    // grid

    var grid = new UI.Number(1).setWidth('40px').onChange(update);
    //gridButtons.add(new UI.Text('grid: '));
    //gridButtons.add(grid);

    var snap = new UI.THREE.Boolean(false, 'snap').onChange(updateTool);
    //gridButtons.add(snap);

    var local = new UI.THREE.Boolean(false, 'local').onChange(updateTool);
    //gridButtons.add( local );

    var showGrid = new UI.THREE.Boolean(true, 'show').onChange(function () {

        editor.sceneHelpers.visible = showGrid.getValue();

    });
    //gridButtons.add( showGrid );

    function updateTool() {

        signals.snapChanged.dispatch(snap.getValue() === true ? grid.getValue() : null);
        signals.spaceChanged.dispatch(local.getValue() === true ? "local" : "world");
        signals.showGridChanged.dispatch(showGrid.getValue());

    }

    //

    function updateScaleX() {

        var object = editor.selected;

        if (objectScaleLock.getValue() === true) {

            var scale = objectScaleX.getValue() / object.scale.x;

            objectScaleY.setValue(objectScaleY.getValue() * scale);
            objectScaleZ.setValue(objectScaleZ.getValue() * scale);

        }

        update();

    }

    function updateScaleY() {

        var object = editor.selected;

        if (objectScaleLock.getValue() === true) {

            var scale = objectScaleY.getValue() / object.scale.y;

            objectScaleX.setValue(objectScaleX.getValue() * scale);
            objectScaleZ.setValue(objectScaleZ.getValue() * scale);

        }

        update();

    }

    function updateScaleZ() {

        var object = editor.selected;

        if (objectScaleLock.getValue() === true) {

            var scale = objectScaleZ.getValue() / object.scale.z;

            objectScaleX.setValue(objectScaleX.getValue() * scale);
            objectScaleY.setValue(objectScaleY.getValue() * scale);

        }

        update();

    }

    function update() {

        var object = editor.selected;

        if (object !== null) {

            var newPosition = new THREE.Vector3(objectPositionX.getValue(), objectPositionY.getValue(), objectPositionZ.getValue());
            if (object.position.distanceTo(newPosition) >= 0.01) {

                editor.execute(new SetPositionCommand(object, newPosition));

            }

            var newRotation = new THREE.Euler(objectRotationX.getValue() * THREE.Math.DEG2RAD, objectRotationY.getValue() * THREE.Math.DEG2RAD, objectRotationZ.getValue() * THREE.Math.DEG2RAD);
            if (object.rotation.toVector3().distanceTo(newRotation.toVector3()) >= 0.01) {

                editor.execute(new SetRotationCommand(object, newRotation));

            }

            var newScale = new THREE.Vector3(objectScaleX.getValue(), objectScaleY.getValue(), objectScaleZ.getValue());
            if (object.scale.distanceTo(newScale) >= 0.01) {

                editor.execute(new SetScaleCommand(object, newScale));

            }

            if (object.fov !== undefined && Math.abs(object.fov - objectFov.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'fov', objectFov.getValue()));
                object.updateProjectionMatrix();

            }

            if (object.near !== undefined && Math.abs(object.near - objectNear.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'near', objectNear.getValue()));

            }

            if (object.far !== undefined && Math.abs(object.far - objectFar.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'far', objectFar.getValue()));

            }

            if (object.intensity !== undefined && Math.abs(object.intensity - objectIntensity.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'intensity', objectIntensity.getValue()));

            }

            if (object.color !== undefined && object.color.getHex() !== objectColor.getHexValue()) {

                editor.execute(new SetColorCommand(object, 'color', objectColor.getHexValue()));

            }

            if (object.groundColor !== undefined && object.groundColor.getHex() !== objectGroundColor.getHexValue()) {

                editor.execute(new SetColorCommand(object, 'groundColor', objectGroundColor.getHexValue()));

            }

            if (object.distance !== undefined && Math.abs(object.distance - objectDistance.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'distance', objectDistance.getValue()));

            }

            if (object.angle !== undefined && Math.abs(object.angle - objectAngle.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'angle', objectAngle.getValue()));

            }

            if (object.penumbra !== undefined && Math.abs(object.penumbra - objectPenumbra.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'penumbra', objectPenumbra.getValue()));

            }

            if (object.decay !== undefined && Math.abs(object.decay - objectDecay.getValue()) >= 0.01) {

                editor.execute(new SetValueCommand(object, 'decay', objectDecay.getValue()));

            }

            if (object.visible !== objectVisible.getValue()) {

                editor.execute(new SetValueCommand(object, 'visible', objectVisible.getValue()));

            }

            if (object.castShadow !== undefined && object.castShadow !== objectCastShadow.getValue()) {

                editor.execute(new SetValueCommand(object, 'castShadow', objectCastShadow.getValue()));

            }

            if (object.receiveShadow !== undefined && object.receiveShadow !== objectReceiveShadow.getValue()) {

                editor.execute(new SetValueCommand(object, 'receiveShadow', objectReceiveShadow.getValue()));
                object.material.needsUpdate = true;

            }

            if (object.shadow !== undefined) {

                if (object.shadow.radius !== objectShadowRadius.getValue()) {

                    editor.execute(new SetValueCommand(object.shadow, 'radius', objectShadowRadius.getValue()));

                }

            }

            try {

                var userData = JSON.parse(objectUserData.getValue());
                if (JSON.stringify(object.userData) != JSON.stringify(userData)) {

                    editor.execute(new SetValueCommand(object, 'userData', userData));

                }

            } catch (exception) {

                console.warn(exception);

            }

        }

    }

    function updateRows(object) {

        var properties = {
            'fov': objectFovRow,
            'near': objectNearRow,
            'far': objectFarRow,
            'intensity': objectIntensityRow,
            'color': objectColorRow,
            'groundColor': objectGroundColorRow,
            'distance': objectDistanceRow,
            'angle': objectAngleRow,
            'penumbra': objectPenumbraRow,
            'decay': objectDecayRow,
            'castShadow': objectShadowRow,
            'receiveShadow': objectReceiveShadow,
            'shadow': objectShadowRadius
        };

        for (var property in properties) {

            properties[property].setDisplay(object[property] !== undefined ? '' : 'none');

        }

    }

    function updateTransformRows(object) {

        if (object instanceof THREE.Light ||
            ( object instanceof THREE.Object3D && object.userData.targetInverse )) {

            objectRotationRow.setDisplay('none');
            objectScaleRow.setDisplay('none');

        } else {

            objectRotationRow.setDisplay('');
            objectScaleRow.setDisplay('');

        }

    }

    // events

    signals.objectSelected.add(function (object) {

        if (object !== null) {

            container.setDisplay('block');

            updateRows(object);
            updateUI(object);

        } else {

            container.setDisplay('none');

        }

    });

    function checkBounds(object) {
        function roundPlus(x, n) { //x - число, n - количество знаков
            if (isNaN(x) || isNaN(n)) return false;
            var m = Math.pow(10, n);
            return Math.round(x * m) / m;
        }

        var box, box_size, position;
        var bigger, out;
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        bigger = false;
        out = false;
        box = new THREE.Box3().setFromObject(object);
        box_size = box.size();
        box_size.x = roundPlus(box_size.x, 1);
        box_size.y = roundPlus(box_size.y, 1);
        box_size.z = roundPlus(box_size.z, 1);

        bigger = (box_size.x > workspace_size || box_size.y > workspace_size || box_size.z > workspace_size);

        position = object.position;
        position.x = roundPlus(position.x, 1);
        position.y = roundPlus(position.y, 1);
        position.z = roundPlus(position.z, 1);

        var tmp1 = (position.x + box_size.x / 2 > workspace_size / 2); // > x
        var tmp2 = (position.y + box_size.y / 2 > workspace_size);   // > y
        var tmp3 = (position.z + box_size.z / 2 > workspace_size / 2); // > z

        var tmp4 = ((-1) * position.x + box_size.x / 2 > workspace_size / 2); //  < x
        var tmp5 = (position.y < box_size.y / 2);                       //  < y
        var tmp6 = ((-1) * position.z + box_size.z / 2 > workspace_size / 2); //  < z

        out = tmp1 || tmp2 || tmp3 || tmp4 || tmp5 || tmp6;

        if (out || bigger) {
            editor.execute(new SetMaterialColorCommand(object, 'emissive', "0xff0000"));
        } else {
            editor.execute(new SetMaterialColorCommand(object, 'emissive', "0x000000"));
        }
    }

    function itemWalker(item, MyFunc) {
        if (item.children[0]) {
            var i = 0;
            while (item.children[i]) {
                itemWalker(item.children[i], MyFunc);
                i++;
            }
        } else {
            if(item instanceof THREE.Group){
                return;
            }else{
                MyFunc(item);
            }
        }

    }

    signals.objectChanged.add(function (object) {

        if (object !== editor.selected) return;

        //start checkboud code
        itemWalker(object, checkBounds);

        updateUI(object);

    });

    signals.refreshSidebarObject3D.add(function (object) {

        if (object !== editor.selected) return;

        updateUI(object);

    });

    function updateUI(object) {

        objectType.setValue(object.type);

        objectUUID.setValue(object.uuid);
        objectName.setValue(object.name);

        objectPositionX.setValue(object.position.x);
        objectPositionY.setValue(object.position.y);
        objectPositionZ.setValue(object.position.z);

        objectRotationX.setValue(object.rotation.x * THREE.Math.RAD2DEG);
        objectRotationY.setValue(object.rotation.y * THREE.Math.RAD2DEG);
        objectRotationZ.setValue(object.rotation.z * THREE.Math.RAD2DEG);

        objectScaleX.setValue(object.scale.x);
        objectScaleY.setValue(object.scale.y);
        objectScaleZ.setValue(object.scale.z);

        if (object.fov !== undefined) {

            objectFov.setValue(object.fov);

        }

        if (object.near !== undefined) {

            objectNear.setValue(object.near);

        }

        if (object.far !== undefined) {

            objectFar.setValue(object.far);

        }

        if (object.intensity !== undefined) {

            objectIntensity.setValue(object.intensity);

        }

        if (object.color !== undefined) {

            objectColor.setHexValue(object.color.getHexString());

        }

        if (object.groundColor !== undefined) {

            objectGroundColor.setHexValue(object.groundColor.getHexString());

        }

        if (object.distance !== undefined) {

            objectDistance.setValue(object.distance);

        }

        if (object.angle !== undefined) {

            objectAngle.setValue(object.angle);

        }

        if (object.penumbra !== undefined) {

            objectPenumbra.setValue(object.penumbra);

        }

        if (object.decay !== undefined) {

            objectDecay.setValue(object.decay);

        }

        if (object.castShadow !== undefined) {

            objectCastShadow.setValue(object.castShadow);

        }

        if (object.receiveShadow !== undefined) {

            objectReceiveShadow.setValue(object.receiveShadow);

        }

        if (object.shadow !== undefined) {

            objectShadowRadius.setValue(object.shadow.radius);

        }

        objectVisible.setValue(object.visible);

        try {

            objectUserData.setValue(JSON.stringify(object.userData, null, '  '));

        } catch (error) {

            console.log(error);

        }

        objectUserData.setBorderColor('transparent');
        objectUserData.setBackgroundColor('');

        updateTransformRows(object);

    }

    var alignRow = new UI.Row();
    var alignText = new UI.Text("Aligment");
    var alignButtons = new UI.Panel();
    alignRow.add(alignText);
    alignRow.add(alignButtons);
    container.add(alignRow);

    // alignXY
    var alignXY = new UI.Button('').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.z = ((box.size()).z / 2 - workspace_size / 2 + editor.selected.position.z - box.center().z);

        objectPositionZ.setValue(editor.selected.position.Z);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignXY.setId('alignXYBtn');
    alignButtons.add(alignXY);

    // alignXZ
    var alignXZ = new UI.Button('').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.y = ((box.size().y / 2) + editor.selected.position.y - box.center().y);

        objectPositionY.setValue(editor.selected.position.x);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignXZ.setId('alignXZBtn');
    alignButtons.add(alignXZ);

    // alignYZ
    var alignYZ = new UI.Button('').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.x = ((box.size()).x / 2 - workspace_size / 2 + editor.selected.position.x - box.center().x);

        objectPositionX.setValue(editor.selected.position.x);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    alignYZ.setId('alignYZBtn');
    alignButtons.add(alignYZ);

    // alignCenter
    var alignCenter = new UI.Button('alignCenter').onClick(function () {

        var box = new THREE.Box3().setFromObject(editor.selected);
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);
        editor.selected.position.x = (0 + editor.selected.position.x - box.center().x);
        editor.selected.position.y = (workspace_size / 2 + editor.selected.position.y - box.center().y);
        editor.selected.position.z = (0 + editor.selected.position.z - box.center().z);

        objectPositionX.setValue(editor.selected.position.x);
        objectPositionY.setValue(editor.selected.position.y);
        objectPositionZ.setValue(editor.selected.position.z);

        var newPosition = new THREE.Vector3(editor.selected.position.x, editor.selected.position.y, editor.selected.position.z);

        editor.execute(new SetPositionCommand(editor.selected, newPosition));

    });
    //alignCenter.setId( 'alignCenterBtn' );
    alignButtons.add(alignCenter);

    //scale panel
    var scaleTWRow = new UI.Row();
    var scaleText = new UI.Text("Scaling");
    var scaleTWButtons = new UI.Panel();
    scaleTWRow.add(scaleText);
    scaleTWRow.add(scaleTWButtons);
    container.add(scaleTWRow);

    // scaleToWorkspace
    var scaleTW = new UI.Button('').onClick(function () {
        var newScale = 1;
        var workspace_size = Math.floor(editor.config.getKey('workspace_size') * 10);


        var box = new THREE.Box3().setFromObject(editor.selected);

        var dim = box.size();
        var dim_compare = {};
        dim_compare.key = 'x';
        dim_compare.value = dim.x;

        if (dim.x < dim.y) {
            dim_compare.key = 'y';
            dim_compare.value = dim.y;
        }

        if (dim.z > dim_compare.value) {
            dim_compare.key = 'z';
            dim_compare.value = dim.z;
        }

        newScale = workspace_size / dim_compare.value;

        editor.selected.scale.x *= newScale;
        editor.selected.scale.y *= newScale;
        editor.selected.scale.z *= newScale;

        var box = new THREE.Box3().setFromObject(editor.selected);

        editor.selected.position.x = (0 + editor.selected.position.x - box.center().x);
        editor.selected.position.y = ((box.size().y / 2) + editor.selected.position.y - box.center().y);
        editor.selected.position.z = (0 + editor.selected.position.z - box.center().z);

        objectScaleX.setValue(newScale);
        objectScaleY.setValue(newScale);
        objectScaleZ.setValue(newScale);

        editor.execute(new SetScaleCommand(editor.selected, new THREE.Vector3(editor.selected.scale.x, editor.selected.scale.y, editor.selected.scale.z)));

    });
    scaleTW.setId('scaleTWBtn');
    scaleTWButtons.add(scaleTW);

    return container;

}