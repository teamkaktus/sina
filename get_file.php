<?php
$filename = "Money_Clip-01.STL";
$ctype = 'text/plain';
if (isset($filename) and file_exists($filename)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($filename));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    if (ob_get_level()) {
        ob_end_clean();
    }
    readfile($filename);
    exit();
} else {
    echo false;
    exit;
}
