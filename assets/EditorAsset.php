<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EditorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'editor/css/main.css',
        'editor/css/light.css',
        'editor/js/libs/codemirror/codemirror.css',
        'editor/js/libs/codemirror/theme/monokai.css',
        'editor/js/libs/codemirror/addon/dialog.css',
        'editor/js/libs/codemirror/addon/show-hint.css',
        'editor/js/libs/codemirror/addon/tern.css'
    ];
    public $js = [
        'js/jquery-2.1.3.min.js',

        'editor/build/three.js',
        'editor/examples/js/libs/system.min.js',

        'editor/examples/js/controls/EditorControls.js',
        'editor/examples/js/controls/TransformControls.js',

        'editor/examples/js/libs/jszip.min.js',
        'editor/examples/js/loaders/AMFLoader.js',
        'editor/examples/js/loaders/AWDLoader.js',
        'editor/examples/js/loaders/BabylonLoader.js',
        'editor/examples/js/loaders/ColladaLoader2.js',
        'editor/examples/js/loaders/FBXLoader.js',
        'editor/examples/js/loaders/GLTFLoader.js',
        'editor/examples/js/loaders/KMZLoader.js',
        'editor/examples/js/loaders/MD2Loader.js',
        'editor/examples/js/loaders/OBJLoader.js',
        'editor/examples/js/loaders/PlayCanvasLoader.js',
        'editor/examples/js/loaders/PLYLoader.js',
        'editor/examples/js/loaders/STLLoader.js',
        'editor/examples/js/loaders/TGALoader.js',
        'editor/examples/js/loaders/UTF8Loader.js',
        'editor/examples/js/loaders/VRMLLoader.js',
        'editor/examples/js/loaders/VTKLoader.js',
        'editor/examples/js/loaders/ctm/lzma.js',
        'editor/examples/js/loaders/ctm/ctm.js',
        'editor/examples/js/loaders/ctm/CTMLoader.js',
        'editor/examples/js/exporters/OBJExporter.js',
        'editor/examples/js/exporters/STLExporter.js',

        'editor/examples/js/loaders/deprecated/SceneLoader.js',

        'editor/examples/js/renderers/Projector.js',
        'editor/examples/js/renderers/CanvasRenderer.js',
        'editor/examples/js/renderers/RaytracingRenderer.js',
        'editor/examples/js/renderers/SoftwareRenderer.js',
        'editor/examples/js/renderers/SVGRenderer.js',
        'editor/js/libs/codemirror/codemirror.js',
        'editor/js/libs/codemirror/mode/javascript.js',
        'editor/js/libs/codemirror/mode/glsl.js',

        'editor/js/libs/esprima.js',
        'editor/js/libs/jsonlint.js',
        'editor/js/libs/glslprep.min.js',
        'editor/js/libs/codemirror/addon/dialog.js',
        'editor/js/libs/codemirror/addon/show-hint.js',
        'editor/js/libs/codemirror/addon/tern.js',
        'editor/js/libs/acorn/acorn.js',
        'editor/js/libs/acorn/acorn_loose.js',
        'editor/js/libs/acorn/walk.js',
        'editor/js/libs/ternjs/polyfill.js',
        'editor/js/libs/ternjs/signal.js',
        'editor/js/libs/ternjs/tern.js',
        'editor/js/libs/ternjs/def.js',
        'editor/js/libs/ternjs/comment.js',
        'editor/js/libs/ternjs/infer.js',
        'editor/js/libs/ternjs/doc_comment.js',
        'editor/js/libs/tern-threejs/threejs.js',

        'editor/js/libs/signals.min.js',
        'editor/js/libs/ui.js',
        'editor/js/libs/ui.three.js',

        'editor/js/libs/app.js',
        'editor/js/Player.js',
        'editor/js/Script.js',

        'editor/examples/js/effects/VREffect.js',
        'editor/examples/js/controls/VRControls.js',
        'editor/examples/js/WebVR.js',

        'editor/js/Storage.js',

        'editor/js/Editor.js',
        'editor/js/Config.js',
        'editor/js/History.js',
        'editor/js/Loader.js',
        'editor/js/Menubar.js',
        'editor/js/Menubar.File.js',
        'editor/js/Menubar.Edit.js',
        'editor/js/Menubar.Add.js',
        'editor/js/Menubar.Play.js',
        'editor/js/Menubar.View.js',
        'editor/js/Menubar.Help.js',
        'editor/js/Menubar.Status.js',
        'editor/js/Sidebar.js',
        'editor/js/Sidebar.BasicControlls.js',
        'editor/js/Sidebar.AdvancedControlls.js',
        'editor/js/Sidebar.Scene.js',
        'editor/js/Sidebar.MyScene.js',
        'editor/js/Sidebar.Project.js',
        'editor/js/Sidebar.Settings.js',
        'editor/js/Sidebar.Properties.js',
        'editor/js/Sidebar.Object.js',
        'editor/js/Sidebar.Geometry.js',
        'editor/js/Sidebar.Geometry.Geometry.js',
        'editor/js/Sidebar.Geometry.BufferGeometry.js',
        'editor/js/Sidebar.Geometry.Modifiers.js',
        'editor/js/Sidebar.Geometry.BoxGeometry.js',
        'editor/js/Sidebar.Geometry.CircleGeometry.js',
        'editor/js/Sidebar.Geometry.CylinderGeometry.js',
        'editor/js/Sidebar.Geometry.IcosahedronGeometry.js',
        'editor/js/Sidebar.Geometry.PlaneGeometry.js',
        'editor/js/Sidebar.Geometry.SphereGeometry.js',
        'editor/js/Sidebar.Geometry.TorusGeometry.js',
        'editor/js/Sidebar.Geometry.TorusKnotGeometry.js',
        'editor/examples/js/geometries/TeapotBufferGeometry.js',
        'editor/js/Sidebar.Geometry.TeapotBufferGeometry.js',
        'editor/js/Sidebar.Geometry.LatheGeometry.js',
        'editor/js/Sidebar.Material.js',
        'editor/js/Sidebar.Animation.js',
        'editor/js/Sidebar.Script.js',
        'editor/js/Sidebar.History.js',
        'editor/js/Toolbar.js',
        'editor/js/Viewport.js',
        'editor/js/Viewport.Info.js',
        'editor/js/Command.js',
        'editor/js/commands/AddObjectCommand.js',
        'editor/js/commands/RemoveObjectCommand.js',
        'editor/js/commands/MoveObjectCommand.js',
        'editor/js/commands/SetPositionCommand.js',
        'editor/js/commands/SetRotationCommand.js',
        'editor/js/commands/SetScaleCommand.js',
        'editor/js/commands/SetValueCommand.js',
        'editor/js/commands/SetUuidCommand.js',
        'editor/js/commands/SetColorCommand.js',
        'editor/js/commands/SetGeometryCommand.js',
        'editor/js/commands/SetGeometryValueCommand.js',
        'editor/js/commands/MultiCmdsCommand.js',
        'editor/js/commands/AddScriptCommand.js',
        'editor/js/commands/RemoveScriptCommand.js',
        'editor/js/commands/SetScriptValueCommand.js',
        'editor/js/commands/SetMaterialCommand.js',
        'editor/js/commands/SetMaterialValueCommand.js',
        'editor/js/commands/SetMaterialColorCommand.js',
        'editor/js/commands/SetMaterialMapCommand.js',
        'editor/js/commands/SetSceneCommand.js',

        'editor/js/libs/html2canvas.js',
        'editor/js/libs/three.html.js',
        'editor/main_index.js',

        //csg
        'editor/js/csg/csg.js',
        'editor/js/csg/ThreeCSG.js',
        'editor/js/csg/THREEx.GeometryUtils.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
