<?php
/**
 * Created by PhpStorm.
 * User: lexlazzy
 * Date: 29.07.2016
 * Time: 15:34
 */
use app\assets\EditorAsset;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
EditorAsset::register($this);

$this->title = 'Editor';
$this->params['breadcrumbs'][] = $this->title;
?>
<link id="theme" rel="stylesheet" />
