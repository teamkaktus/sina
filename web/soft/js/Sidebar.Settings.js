/**
 * @author mrdoob / http://mrdoob.com/
 */

Sidebar.Settings = function ( editor ) {

	var config = editor.config;
	var signals = editor.signals;

	var container = new UI.Panel();
	container.setBorderTop( '0' );
	container.setPaddingTop( '20px' );

	// class

	var options = {
		'css/light.css': 'light',
		'css/dark.css': 'dark'
	};

	var themeRow = new UI.Row();
	var theme = new UI.Select().setWidth( '150px' );
	theme.setOptions( options );

	if ( config.getKey( 'theme' ) !== undefined ) {

		theme.setValue( config.getKey( 'theme' ) );

	}

	theme.onChange( function () {

		var value = this.getValue();

		editor.setTheme( value );
		editor.config.setKey( 'theme', value );

	} );

	themeRow.add( new UI.Text( 'Theme' ).setWidth( '90px' ) );
	//themeRow.add( theme );

	container.add( themeRow );

    var sizeRow = new UI.Row();
    var buttons = new UI.Panel();
    sizeRow.add(buttons);
    container.add( sizeRow );

    var sizeLabel = new UI.Text( 'size').setWidth('50px');
    var unitLabel = new UI.Text( 'inch');

    var size = new UI.Input().setValue(editor.config.getKey('workspace_size')).setWidth('50px');
    size.dom.id = 'size';


    var setSize = new UI.Button( 'set size' ).onClick( function () {
        var val = document.getElementById('size').value*10;
		if(val% 2 == 1){
			val-=1
		}
        editor.config.setKey('workspace_size', val/10);
        editor.setSize( val/10 );
		console.log(val);

    } );

    setSize.dom.style.margin = '0 0 0 10px';
    buttons.add( setSize );

	sizeRow.add( sizeLabel );
	sizeRow.add( size );
	sizeRow.add( unitLabel );
	sizeRow.add( setSize );

	container.add( sizeRow );

	return container;

};
