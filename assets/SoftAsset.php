<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

    namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoftAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'soft/css/main.css',
        'soft/css/light.css',
        'soft/js/libs/codemirror/codemirror.css',
        'soft/js/libs/codemirror/theme/monokai.css',
        'soft/js/libs/codemirror/addon/dialog.css',
        'soft/js/libs/codemirror/addon/show-hint.css',
        'soft/js/libs/codemirror/addon/tern.css'
    ];
    public $js = [
        'js/jquery-2.1.3.min.js',

        'soft/build/three.js',
        'soft/examples/js/libs/system.min.js',

        'soft/examples/js/controls/EditorControls.js',
        'soft/examples/js/controls/TransformControls.js',

        'soft/examples/js/libs/jszip.min.js',
        'soft/examples/js/loaders/AMFLoader.js',
        'soft/examples/js/loaders/AWDLoader.js',
        'soft/examples/js/loaders/BabylonLoader.js',
        'soft/examples/js/loaders/ColladaLoader2.js',
        'soft/examples/js/loaders/FBXLoader.js',
        'soft/examples/js/loaders/GLTFLoader.js',
        'soft/examples/js/loaders/KMZLoader.js',
        'soft/examples/js/loaders/MD2Loader.js',
        'soft/examples/js/loaders/OBJLoader.js',
        'soft/examples/js/loaders/PlayCanvasLoader.js',
        'soft/examples/js/loaders/PLYLoader.js',
        'soft/examples/js/loaders/STLLoader.js',
        'soft/examples/js/loaders/TGALoader.js',
        'soft/examples/js/loaders/UTF8Loader.js',
        'soft/examples/js/loaders/VRMLLoader.js',
        'soft/examples/js/loaders/VTKLoader.js',
        'soft/examples/js/loaders/ctm/lzma.js',
        'soft/examples/js/loaders/ctm/ctm.js',
        'soft/examples/js/loaders/ctm/CTMLoader.js',
        'soft/examples/js/exporters/OBJExporter.js',
        'soft/examples/js/exporters/STLExporter.js',

        'soft/examples/js/loaders/deprecated/SceneLoader.js',

        'soft/examples/js/renderers/Projector.js',
        'soft/examples/js/renderers/CanvasRenderer.js',
        'soft/examples/js/renderers/RaytracingRenderer.js',
        'soft/examples/js/renderers/SoftwareRenderer.js',
        'soft/examples/js/renderers/SVGRenderer.js',
        'soft/js/libs/codemirror/codemirror.js',
        'soft/js/libs/codemirror/mode/javascript.js',
        'soft/js/libs/codemirror/mode/glsl.js',

        'soft/js/libs/esprima.js',
        'soft/js/libs/jsonlint.js',
        'soft/js/libs/glslprep.min.js',
        'soft/js/libs/codemirror/addon/dialog.js',
        'soft/js/libs/codemirror/addon/show-hint.js',
        'soft/js/libs/codemirror/addon/tern.js',
        'soft/js/libs/acorn/acorn.js',
        'soft/js/libs/acorn/acorn_loose.js',
        'soft/js/libs/acorn/walk.js',
        'soft/js/libs/ternjs/polyfill.js',
        'soft/js/libs/ternjs/signal.js',
        'soft/js/libs/ternjs/tern.js',
        'soft/js/libs/ternjs/def.js',
        'soft/js/libs/ternjs/comment.js',
        'soft/js/libs/ternjs/infer.js',
        'soft/js/libs/ternjs/doc_comment.js',
        'soft/js/libs/tern-threejs/threejs.js',

        'soft/js/libs/signals.min.js',
        'soft/js/libs/ui.js',
        'soft/js/libs/ui.three.js',

        'soft/js/libs/app.js',
        'soft/js/Player.js',
        'soft/js/Script.js',

        'soft/examples/js/effects/VREffect.js',
        'soft/examples/js/controls/VRControls.js',
        'soft/examples/js/WebVR.js',

        'soft/js/Storage.js',

        'soft/js/Editor.js',
        'soft/js/Config.js',
        'soft/js/History.js',
        'soft/js/Loader.js',
        'soft/js/Menubar.js',
        'soft/js/Menubar.File.js',
        'soft/js/Menubar.Edit.js',
        'soft/js/Menubar.Add.js',
        'soft/js/Menubar.Play.js',
        'soft/js/Menubar.View.js',
        'soft/js/Menubar.Help.js',
        'soft/js/Menubar.Status.js',
        'soft/js/Sidebar.js',
        'soft/js/Sidebar.BasicControlls.js',
        'soft/js/Sidebar.AdvancedControlls.js',
        'soft/js/Sidebar.Scene.js',
        'soft/js/Sidebar.MyScene.js',
        'soft/js/Sidebar.Project.js',
        'soft/js/Sidebar.Settings.js',
        'soft/js/Sidebar.Properties.js',
        'soft/js/Sidebar.Object.js',
        'soft/js/Sidebar.Geometry.js',
        'soft/js/Sidebar.Geometry.Geometry.js',
        'soft/js/Sidebar.Geometry.BufferGeometry.js',
        'soft/js/Sidebar.Geometry.Modifiers.js',
        'soft/js/Sidebar.Geometry.BoxGeometry.js',
        'soft/js/Sidebar.Geometry.CircleGeometry.js',
        'soft/js/Sidebar.Geometry.CylinderGeometry.js',
        'soft/js/Sidebar.Geometry.IcosahedronGeometry.js',
        'soft/js/Sidebar.Geometry.PlaneGeometry.js',
        'soft/js/Sidebar.Geometry.SphereGeometry.js',
        'soft/js/Sidebar.Geometry.TorusGeometry.js',
        'soft/js/Sidebar.Geometry.TorusKnotGeometry.js',
        'soft/examples/js/geometries/TeapotBufferGeometry.js',
        'soft/js/Sidebar.Geometry.TeapotBufferGeometry.js',
        'soft/js/Sidebar.Geometry.LatheGeometry.js',
        'soft/js/Sidebar.Material.js',
        'soft/js/Sidebar.Animation.js',
        'soft/js/Sidebar.Script.js',
        'soft/js/Sidebar.History.js',
        'soft/js/Toolbar.js',
        'soft/js/Viewport.js',
        'soft/js/Viewport.Info.js',
        'soft/js/Command.js',
        'soft/js/commands/AddObjectCommand.js',
        'soft/js/commands/RemoveObjectCommand.js',
        'soft/js/commands/MoveObjectCommand.js',
        'soft/js/commands/SetPositionCommand.js',
        'soft/js/commands/SetRotationCommand.js',
        'soft/js/commands/SetScaleCommand.js',
        'soft/js/commands/SetValueCommand.js',
        'soft/js/commands/SetUuidCommand.js',
        'soft/js/commands/SetColorCommand.js',
        'soft/js/commands/SetGeometryCommand.js',
        'soft/js/commands/SetGeometryValueCommand.js',
        'soft/js/commands/MultiCmdsCommand.js',
        'soft/js/commands/AddScriptCommand.js',
        'soft/js/commands/RemoveScriptCommand.js',
        'soft/js/commands/SetScriptValueCommand.js',
        'soft/js/commands/SetMaterialCommand.js',
        'soft/js/commands/SetMaterialValueCommand.js',
        'soft/js/commands/SetMaterialColorCommand.js',
        'soft/js/commands/SetMaterialMapCommand.js',
        'soft/js/commands/SetSceneCommand.js',

        'soft/js/libs/html2canvas.js',
        'soft/js/libs/three.html.js',
        'soft/main_index.js',

        //csg
        'soft/js/csg/csg.js',
        'soft/js/csg/ThreeCSG.js',
        'soft/js/csg/THREEx.GeometryUtils.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
